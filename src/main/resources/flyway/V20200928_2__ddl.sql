insert into groups(group_name) values
('first'),
('second'),
('third'),
('forth'),
('fifth');

insert into students (student_name,student_phone,group_id) values
('Yato','879',3),
('Totoro','87985',2),
('Minato','87459',4),
('Buaga','4569',3),
('Telepuzik','8479',1),
('None','879',1);
