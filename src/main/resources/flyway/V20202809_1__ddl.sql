 create table groups (
group_id serial PRIMARY KEY ,
group_name varchar (255)
);

Create table students (
student_id serial PRIMARY KEY ,
student_name varchar (255),
student_phone varchar (255),
group_id int
);