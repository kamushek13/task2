package kz.aitu.java.task2.controller;


import kz.aitu.java.task2.repository.GroupRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GroupContoller {
    private final GroupRepository groupRepository;

    public GroupContoller(GroupRepository groupRepository){
        this.groupRepository = groupRepository;
    }

    @GetMapping ("/api/group")
    public ResponseEntity<?> getGroup(){
        return ResponseEntity.ok(groupRepository.findAll());
    }
}
