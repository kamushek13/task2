package kz.aitu.java.task2.controller;


import kz.aitu.java.task2.repository.StudentRepository;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {
    private final StudentRepository studentRepository;

    public StudentController(StudentRepository studentRepository){
        this.studentRepository= studentRepository;
    }

    @GetMapping("api/student")
    public ResponseEntity<?> getStudent(){
        return ResponseEntity.ok(studentRepository.findAll());

    }

}
