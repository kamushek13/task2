package kz.aitu.java.task2.repository;

import kz.aitu.java.task2.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository <Student, Long> {
    @Query(value = "SELECT * FROM students GROUP BY group_id", nativeQuery = true)
    Student getStudent();

}
