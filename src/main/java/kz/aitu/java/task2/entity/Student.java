package kz.aitu.java.task2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table (name = "students")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    @Id @GeneratedValue (strategy = GenerationType.AUTO)
    private long id;
    @Column (name = "student_name")
    private String name;
    @Column(name = "student_phone")
    private String phone;

    @ManyToOne
    @JoinColumn (name = "group_id")
    private Group groupId;

}
