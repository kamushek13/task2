package kz.aitu.java.task2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.*;

@Entity
@Table(name = "groups")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Group {

    @Id @GeneratedValue (strategy = GenerationType.AUTO)
    private long id;

    @Column (name = "group_name")
    private String name;
}
